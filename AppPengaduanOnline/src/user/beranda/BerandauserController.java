/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.beranda;

import entity.InformasiCovid;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.ResponseInformasiCovid;
import network.APIClient;
import network.APIServices;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class BerandauserController implements Initializable {

    @FXML
    private TableView<InformasiCovid> table;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  sembuh;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  meninggal;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  pos;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  odp;
    
     @FXML 
    private LineChart<String,Number> lineChart;
     
    @FXML
    private CategoryAxis cAxis;
     
    @FXML
    private NumberAxis nAxis;
    
    @FXML
    private TextArea isiKeluhan;
    
    @FXML
    RadioButton ya1, ya2, ya3, ya4, no1, no2, no3, no4;
    
    final ToggleGroup group1 = new ToggleGroup();
    final ToggleGroup group2 = new ToggleGroup();
    final ToggleGroup group3 = new ToggleGroup();
    final ToggleGroup group4 = new ToggleGroup();
    
    ObservableList<InformasiCovid> oblist = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ya1.setToggleGroup(group1);
        no1.setToggleGroup(group1);
        ya2.setToggleGroup(group2);
        no2.setToggleGroup(group2);
        ya3.setToggleGroup(group3);
        no3.setToggleGroup(group3);
        ya4.setToggleGroup(group4);
        no4.setToggleGroup(group4);
        
        getData();
        dataBarchart();
    } 
    public void dataBarchart(){
          APIServices services = APIClient.getRetrofit().create(APIServices.class);
          Call<ResponseInformasiCovid> call = services.getInfoCovid();
          
          call.enqueue(new Callback<ResponseInformasiCovid>(){
              @Override
              public void onResponse(Call<ResponseInformasiCovid> call, Response<ResponseInformasiCovid> rspns) {
                   if(rspns.isSuccessful()){
                        int size = rspns.body().getInformasi_covid().size();
                        List<InformasiCovid> s = rspns.body().getInformasi_covid();
                       
//                        cAxis = new CategoryAxis();
                        Platform.runLater(() -> {
                            cAxis.setLabel("Id");
                            
    //                        nAxis = new NumberAxis();
                            nAxis.setLabel("Jumlah");
                          });
//                        lineChart = new LineChart<String,Number>(cAxis,nAxis);
                        
                        XYChart.Series series1 = new XYChart.Series();
                        series1.setName("Sembuh");
                     
                        XYChart.Series series2 = new XYChart.Series();
                        series2.setName("Meninggal");
                        
                        XYChart.Series series3 = new XYChart.Series();
                        series3.setName("Positif");
                        
                        XYChart.Series series4 = new XYChart.Series();
                        series4.setName("ODP");
                        
                        for(int i=0;i<size;i++){
                          series1.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getSembuh()));
                          series2.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getMeninggal()));
                          series3.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getPositif()));
                          series4.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getOdp()));
                        }
                        
                         Platform.runLater(() -> {
                            lineChart.setTitle("Informasi Covid");
                            lineChart.getData().addAll(series1, series2, series3, series4);
                       });
                   }else{
                        System.out.println(rspns.message());
                    }
              }

              @Override
              public void onFailure(Call<ResponseInformasiCovid> call, Throwable thrwbl) {
                  System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
              }
              
          });
     }
    
    public void getData(){
        APIServices services = APIClient.getRetrofit().create(APIServices.class);
        Call<ResponseInformasiCovid> call = services.getInfoCovid();
      
        call.enqueue(new Callback<ResponseInformasiCovid>(){
            @Override
            public void onResponse(Call<ResponseInformasiCovid> call, Response<ResponseInformasiCovid> rspns) {
                 if(rspns.isSuccessful()){
                      int size = rspns.body().getInformasi_covid().size();
                      List<InformasiCovid> s = rspns.body().getInformasi_covid();
                      
                        for(int i=0;i<size;i++){
                            oblist.add(new InformasiCovid(s.get(i).getId(), s.get(i).getSembuh(), s.get(i).getMeninggal(),
                                    s.get(i).getPositif(), s.get(i).getOdp()));
                        }
                        
                        sembuh.setCellValueFactory(new PropertyValueFactory<>("sembuh"));
                        meninggal.setCellValueFactory(new PropertyValueFactory<>("meninggal"));
                        pos.setCellValueFactory(new PropertyValueFactory<>("positif"));
                        odp.setCellValueFactory(new PropertyValueFactory<>("odp"));
                        
                        
                        table.setItems(oblist);
                      
                 }else{
                    System.out.println(rspns.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseInformasiCovid> call, Throwable thrwbl) {
              System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
            }
            
        });
    }
    
    public void logout(ActionEvent event) throws Exception{
        
         Preferences data = Preferences.userRoot();

            data.remove("username");
            data.remove("role");
         try{
         FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/FXMLDocument.fxml"));
         Parent parent1 = pinda.load();

         Scene x = new Scene(parent1);
         Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
         myStage.setScene(x);
         myStage.centerOnScreen();
         myStage.show();

  
         }catch(Exception e){
           e.printStackTrace();
         }
    }
    
    public void about(ActionEvent event) throws Exception{
                                    try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/about/about.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
      }
    
     public void cekCov(ActionEvent event) throws Exception{
           Preferences userPreferences = Preferences.userRoot();
         int hasil = 0;
         if (group1.getSelectedToggle()!=null || group2.getSelectedToggle()!=null || group3.getSelectedToggle()!=null || group4.getSelectedToggle()!=null) {
             RadioButton selectedRadioButton1 = (RadioButton) group1.getSelectedToggle();
         String pilihan1 = selectedRadioButton1.getText();
         RadioButton selectedRadioButton2 = (RadioButton) group2.getSelectedToggle();
         String pilihan2 = selectedRadioButton2.getText();
         RadioButton selectedRadioButton3 = (RadioButton) group3.getSelectedToggle();
         String pilihan3 = selectedRadioButton3.getText();
         RadioButton selectedRadioButton4 = (RadioButton) group4.getSelectedToggle();
         String pilihan4 = selectedRadioButton4.getText();
         
         if(pilihan1.equals("Ya")){
             hasil += 1;
         }
         
         if(pilihan2.equals("Ya")){
             hasil += 1;
         }
         
         if(pilihan3.equals("Ya")){
             hasil += 1;
         }
         
         if(pilihan4.equals("Ya")){
             hasil += 1;
         }
         
         if(hasil==0){
              JOptionPane.showMessageDialog(null, "Hasil test anda rendah :) ", "HEALTY", JOptionPane.INFORMATION_MESSAGE);       
         }else if(hasil>0 && hasil<4){
              JOptionPane.showMessageDialog(null, "Silahkan test anda sedang :| ", "WARNING", JOptionPane.WARNING_MESSAGE);       
         }else if(hasil>=4){
              JOptionPane.showMessageDialog(null, "Silahkan test anda tinggi :( ", "WARNING", JOptionPane.WARNING_MESSAGE);        
         }
         
         String gejala = "0";
         String ak = "1";
         String username = userPreferences.get("username", null);
         
         
          APIServices services = APIClient.getRetrofit().create(APIServices.class);
          services.cekKesehata(gejala, ak, username, hasil)
                  .enqueue(new Callback<ResponseBody>(){
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                            if(rspns.isSuccessful()){
                                Platform.runLater(() -> {
                                try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/user/beranda/berandauser.fxml"));
                                        Parent parent1 = pinda.load();
                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    } 
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                        }
                      
                  });
         
         
         }else{
                 JOptionPane.showMessageDialog(null, "Silahkan isi semua radio diatas", "WARNING", JOptionPane.WARNING_MESSAGE);
        }
         
     
         
     }
     
     public void lapor(ActionEvent event) throws Exception{
          Preferences userPreferences = Preferences.userRoot();
          String username = userPreferences.get("username", null);
          String isi = isiKeluhan.getText().toString();
          
          APIServices services = APIClient.getRetrofit().create(APIServices.class);
          services.addKeluhan(username, "Keluhan", getWaktu(), isi, "mod")
                  .enqueue(new Callback<ResponseBody>(){
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                        if(rspns.isSuccessful()){
                                  JOptionPane.showMessageDialog(null, "Sukses mengirim keluhan ", "Succes", JOptionPane.INFORMATION_MESSAGE);
                                Platform.runLater(() -> {
                                try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/user/beranda/berandauser.fxml"));
                                        Parent parent1 = pinda.load();
                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    } 
                                });
                            }else{
                             System.out.println(rspns.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                        System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                    }
                      
                  });
     }
     
     private String getWaktu() {  
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");  
        Date date = new Date();  
        return dateFormat.format(date);  
    } 
     
     
    
}
