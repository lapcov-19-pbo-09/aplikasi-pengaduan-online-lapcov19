/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apppengaduanonline;

import entity.Account;
import entity.CekKesehatan;
import entity.InformasiCovid;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.ResponseAccount;
import model.ResponseInformasiCovid;
import network.APIClient;
import network.APIServices;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class TambahUserController implements Initializable {

    @FXML
    private TableView<Account> table;  
    
    @FXML
    private TableColumn<Account, String>  cNama;
    
    @FXML
    private TableColumn<Account, String>  cJK;
    
    @FXML
    private TableColumn<Account, String>  cTanggal;
    
    @FXML
    private TableColumn<Account, String>  cAlamat;
    
    @FXML
    private TableColumn<Account, String>  cUsername;
    
    @FXML
    private TableColumn<Account, String>  cPassword;
    
    @FXML
    private TableColumn<Account, String>  cPekerjaan;
    
    @FXML
    private TableColumn<Account, String>  cRole;
    
    @FXML
    private TableColumn<Account, String>  cNik;
    
    @FXML
    private TextField tNama, tAlamat, tUsername, tPassword, tPekerjaan, tNIK;
    
    @FXML
    private DatePicker tTanggal;
    
    @FXML
    private Button bSimpan;
    
    
    @FXML
    RadioButton jkL, jkP, mPengurus, mPenduduk;
    
    final ToggleGroup group = new ToggleGroup();
    final ToggleGroup group1 = new ToggleGroup();
    
    ObservableList<Account> oblist = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        jkL.setToggleGroup(group);
        jkP.setToggleGroup(group);
        mPengurus.setToggleGroup(group1);
        mPenduduk.setToggleGroup(group1);
        getTableUser();
    }
    
    public void getTableUser(){
         APIServices services = APIClient.getRetrofit().create(APIServices.class);
         Call<ResponseAccount> call = services.getUser();
         
         call.enqueue(new Callback<ResponseAccount>(){
             @Override
             public void onResponse(Call<ResponseAccount> call, Response<ResponseAccount> rspns) {
                 if(rspns.isSuccessful()){
                      int size = rspns.body().getAcc().size();
                      List<Account> s = rspns.body().getAcc();
                      for(int i=0;i<size;i++){
                          oblist.add(new Account(s.get(i).getNama(), s.get(i).getJk(), s.get(i).getTanggalLahir(), s.get(i).getAlamat(),
                                s.get(i).getNik(), s.get(i).getUsername(), s.get(i).getPassword(), s.get(i).getPekerjaan(), s.get(i).getFoto(), s.get(i).getRole()));
                      }
                      
                      cNama.setCellValueFactory(new PropertyValueFactory<>("nama"));
                      cJK.setCellValueFactory(new PropertyValueFactory<>("jk"));
                      cTanggal.setCellValueFactory(new PropertyValueFactory<>("tanggalLahir"));
                      cAlamat.setCellValueFactory(new PropertyValueFactory<>("alamat"));
                      cUsername.setCellValueFactory(new PropertyValueFactory<>("username"));
                      cPassword.setCellValueFactory(new PropertyValueFactory<>("password"));
                      cPekerjaan.setCellValueFactory(new PropertyValueFactory<>("pekerjaan"));
                      cRole.setCellValueFactory(new PropertyValueFactory<>("role"));
                      cNik.setCellValueFactory(new PropertyValueFactory<>("nik"));
                      
                      table.setItems(oblist);
                 }else{
                        System.out.println(rspns.message());
                 }
             }

             @Override
             public void onFailure(Call<ResponseAccount> call, Throwable thrwbl) {
                 System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
             }
             
         });
    }
    
     public void kembali(ActionEvent event) throws Exception{
         try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                        Parent parent1 = pinda.load();

                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
     }
     
    public void simpan(ActionEvent event) throws Exception{
         if(table.getSelectionModel().getSelectedItem()!=null){
              APIServices services = APIClient.getRetrofit().create(APIServices.class);
              RadioButton selectedRadioButton = (RadioButton) group.getSelectedToggle();
              String jk = selectedRadioButton.getText();

              RadioButton selectedRadioButton1 = (RadioButton) group1.getSelectedToggle();
              String role = selectedRadioButton1.getText();
              
              String nama =  tNama.getText().toString();
    //        final DatePicker datePicker = new DatePicker(LocalDate.now());
             String tanggalLahir = String.valueOf(tTanggal.getValue());

             String alamat = tAlamat.getText().toString();
             String username = tUsername.getText().toString();
             String password = tPassword.getText().toString();
             String pekerjaan = tPekerjaan.getText().toString();
             String nik = tNIK.getText().toString();
             String foto = "default.png";
             if(alamat.isEmpty() || username.isEmpty() || password.isEmpty() || pekerjaan.isEmpty() || nik.isEmpty() ||
             tanggalLahir.isEmpty() || role.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Semua Data harus disii dengan benar", "WARNING", JOptionPane.WARNING_MESSAGE);                        
            }else{  
                 services.update(nama, jk, tanggalLahir, alamat, nik, username, password, pekerjaan, foto)
                         .enqueue(new Callback<ResponseBody>(){
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                        if(rspns.isSuccessful()){
                             Platform.runLater(() -> {   
                                     try{
                                          FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/TambahUser.fxml"));
                                          Parent parent1 = pinda.load(); 
                                          Scene x = new Scene(parent1);
                                          Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                          myStage.setScene(x);
                                          myStage.centerOnScreen();
                                          myStage.show();

                                              }
                                              catch(Exception e){
                                                  e.printStackTrace();
                                              } 
                              });
                        }else{
                            System.out.println(rspns.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                       System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                    }
                 });
             }
              
         }else{
            
         APIServices services = APIClient.getRetrofit().create(APIServices.class);
         
         RadioButton selectedRadioButton = (RadioButton) group.getSelectedToggle();
         String jk = selectedRadioButton.getText();
         
         RadioButton selectedRadioButton1 = (RadioButton) group1.getSelectedToggle();
         String role = selectedRadioButton1.getText();
         
         String nama =  tNama.getText().toString();
//         final DatePicker datePicker = new DatePicker(LocalDate.now());
         String tanggalLahir = String.valueOf(tTanggal.getValue());
         
         String alamat = tAlamat.getText().toString();
         String username = tUsername.getText().toString();
         String password = tPassword.getText().toString();
         String pekerjaan = tPekerjaan.getText().toString();
         String nik = tNIK.getText().toString();
         String foto = "default.png";
       
         if(alamat.isEmpty() || username.isEmpty() || password.isEmpty() || pekerjaan.isEmpty() || nik.isEmpty() ||
             tanggalLahir.isEmpty() || role.isEmpty()){
                    JOptionPane.showMessageDialog(null, "Semua Data harus disii dengan benar", "WARNING", JOptionPane.WARNING_MESSAGE);                        
            }else{      
         services.regrister(nama, jk, tanggalLahir, alamat, nik, username, password, pekerjaan, foto, role)
                 .enqueue(new Callback<ResponseBody>(){
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                        if(rspns.isSuccessful()){
                             Platform.runLater(() -> {   
                                     try{
                                          FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/TambahUser.fxml"));
                                          Parent parent1 = pinda.load(); 
                                          Scene x = new Scene(parent1);
                                          Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                          myStage.setScene(x);
                                          myStage.centerOnScreen();
                                          myStage.show();

                                              }
                                              catch(Exception e){
                                                  e.printStackTrace();
                                              } 
                              });
                        }else{
                            System.out.println(rspns.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                       System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                    }
                 });
         }
         }
    }
    
     public void deleteUser(ActionEvent event) throws Exception{
         if(table.getSelectionModel().getSelectedItem()!=null){
             Account select = table.getSelectionModel().getSelectedItem();
             APIServices services = APIClient.getRetrofit().create(APIServices.class);
              services.deleteUser(select.getUsername())
                      .enqueue(new Callback<ResponseBody>(){
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                                if(rspns.isSuccessful()){
                                        Platform.runLater(() -> {   
                                                try{
                                                     FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/TambahUser.fxml"));
                                                     Parent parent1 = pinda.load(); 
                                                     Scene x = new Scene(parent1);
                                                     Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                                     myStage.setScene(x);
                                                     myStage.centerOnScreen();
                                                     myStage.show();

                                                         }
                                                         catch(Exception e){
                                                             e.printStackTrace();
                                                         } 
                                         });
                                   }else{
                                       System.out.println(rspns.message());
                                   }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                                System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                            }
                          
                      });
         }
              
      }
     
     public void edit() throws ParseException{
         if(table.getSelectionModel().getSelectedItem()!=null){
             Account select = table.getSelectionModel().getSelectedItem();
             
             tNama.setText(select.getNama());
             tAlamat.setText(select.getAlamat());
             tUsername.setText(select.getUsername());
             tNIK.setText(String.valueOf(select.getNik()));
             tPassword.setText(select.getPassword());
             tPekerjaan.setText(select.getPekerjaan());
             
             bSimpan.setText("Simpan");
             

       
             
//             String tgl1 = tgl.getYear()+"-"+tgl.getMonth()+"-"+tgl.getDay();
//             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
//             Date d = sdf.parse(String.valueOf(select.getTanggalLahir()));
//             System.out.println(d);
                     
//             DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy", this.locale);
             tTanggal.setValue(LOCAL_DATE(String.valueOf(select.getTanggalLahir())));
             
             if(jkL.getText().equals(select.getJk())){
                 jkL.setSelected(true);
             }else if(jkP.getText().equals(select.getJk())){
                 jkP.setSelected(true);
             }
             
             if(mPengurus.getText().equals(select.getRole())){
                 mPengurus.setSelected(true);
             }else if(mPenduduk.getText().equals(select.getRole())){
                 mPenduduk.setSelected(true);
             }
             
             tAlamat.getText().toString();
         String username = tUsername.getText().toString();
         String password = tPassword.getText().toString();
         String pekerjaan = tPekerjaan.getText().toString();
         String nik = tNIK.getText().toString();
         }
     }
     
     public static final LocalDate LOCAL_DATE (String dateString){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate localDate = LocalDate.parse(dateString, formatter);
            return localDate;
        }
     

    
}
