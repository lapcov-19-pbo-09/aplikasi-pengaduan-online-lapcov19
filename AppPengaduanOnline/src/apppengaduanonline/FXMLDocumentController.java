/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apppengaduanonline;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import entity.Account;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import network.APIClient;
import network.APIServices;
import org.json.JSONException;
import org.json.JSONObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 * @author Kevin
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextField username;
    
    @FXML
    private PasswordField password;
    
    @FXML
    Button login;
    
    @FXML
    AnchorPane pane;
  
   
    @FXML
    VBox box;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pane.setVisible(false);
    }    
    
    
    public void login(ActionEvent event) throws Exception{
        
        showDialog();  
        Preferences userPreferences = Preferences.userRoot();
        String sUsername = username.getText().toString();
        String sPassword = password.getText().toString();
        APIServices services = APIClient.getRetrofit().create(APIServices.class);
        Call<JsonObject> call =  services.login(sUsername, sPassword);
        
        call.enqueue(new Callback<JsonObject>(){
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> rspns) {
                if(rspns.isSuccessful()){
                   try {
                     JSONObject jsonObject = new JSONObject(new Gson().toJson(rspns.body()));
                     String success = jsonObject.getString("success");
                      if(success.equals("1") && jsonObject.getString("role").equals("pengurus")){
                          userPreferences.put("username", jsonObject.getString("username"));
                          userPreferences.put("role", jsonObject.getString("role"));
                           hideDialog();
                           Platform.runLater(() -> {   
                                    try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                        Parent parent1 = pinda.load();

                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    });
                          }else if(success.equals("1") && jsonObject.getString("role").equals("penduduk")){
                               userPreferences.put("username", jsonObject.getString("username"));
                               userPreferences.put("role", jsonObject.getString("role"));
                           hideDialog();
                           Platform.runLater(() -> {   
                                    try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/user/beranda/berandauser.fxml"));
                                        Parent parent1 = pinda.load();

                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    });
                          }else{
                              hideDialog();
                             JOptionPane.showMessageDialog(null, "Username/Password Anda tidak terdaftar", "WARNING", JOptionPane.WARNING_MESSAGE);
                          }
                          } catch (JSONException e) {
                                hideDialog();
                                e.printStackTrace();
                          }
                          }else{
                               hideDialog();
                              System.out.println(rspns.message());
                          }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable thrwbl) {
                 hideDialog();
                System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
            }         
        });
    }
    
    
    public void reset(){
        username.setText("");
        password.setText("");
    }
    
    public void showDialog(){
        ProgressIndicator pi = new ProgressIndicator();  
        box = new VBox(pi);
        pane.setVisible(true);   
        box.setAlignment(Pos.CENTER);
        pane.getChildren().add(box);
        username.setDisable(true);
        login.setDisable(true);
        password.setDisable(true);
    }
    
    public void hideDialog(){
        pane.setVisible(false);
        username.setDisable(false);
        login.setDisable(false);
        password.setDisable(false);
    }
}
