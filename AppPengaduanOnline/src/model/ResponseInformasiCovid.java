/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.SerializedName;
import entity.InformasiCovid;
import java.util.List;

/**
 *
 * @author Kevin
 */
public class ResponseInformasiCovid {
    @SerializedName("informasi_covid")
    private List<InformasiCovid> informasi_covid;

    public List<InformasiCovid> getInformasi_covid() {
        return informasi_covid;
    }
    
}
