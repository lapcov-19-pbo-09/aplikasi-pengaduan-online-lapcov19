/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import entity.Account;
import java.util.List;

/**
 *
 * @author Kevin
 */
public class ResponseAccount {
    @SerializedName("account")
    @Expose
    private List<Account> account;

    public List<Account> getAcc(){
        return account;
    }
}
