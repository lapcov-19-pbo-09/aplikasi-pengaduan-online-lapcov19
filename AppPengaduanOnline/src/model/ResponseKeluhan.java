/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import entity.Keluhan;
import java.util.List;

/**
 *
 * @author Kevin
 */
public class ResponseKeluhan {
    @SerializedName("keluhan")
    @Expose
    List<Keluhan> keluhan;

    public List<Keluhan> getKeluhan() {
        return keluhan;
    }

    public void setmKeluhan(List<Keluhan> keluhan) {
        this.keluhan = keluhan;
    }
}
