/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import entity.CekKesehatan;
import java.util.List;

/**
 *
 * @author Kevin
 */
public class ResponseCekKesehatan {
    @SerializedName("cekKesehatan")
    @Expose
    private List<CekKesehatan> cekKesehatan;

    public List<CekKesehatan> getCekKesehatan(){
        return cekKesehatan;
    }
}
