/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import com.google.gson.JsonObject;
import entity.Account;
import model.ResponseAccount;
import model.ResponseCekKesehatan;
import model.ResponseInformasiCovid;
import model.ResponseKeluhan;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 *
 * @author Kevin
 */
public interface APIServices {
    
  @FormUrlEncoded
  @POST("account/login")
  Call<JsonObject> login(@Field("username") String sername,
          @Field("password") String password);
  
  @GET("informasi_covid/index")
  Call<ResponseInformasiCovid> getInfoCovid();
  
  @GET("cekKesehatan/index")
  Call<ResponseCekKesehatan> getHasilCek();
  
  @FormUrlEncoded
  @POST("informasi_covid/tambah")
  Call<ResponseBody> tambah_info(@Field("sembuh") int sembuh,
          @Field("meninggal") int meninggal,
          @Field("positif") int positif,
          @Field("odp") int odp);
  
  
  @DELETE("informasi_covid/delete/{id_informasi}")
  Call<ResponseBody> deleteInfo(@Path("id_informasi")int id_informasi);
  
  @FormUrlEncoded
  @POST("informasi_covid/update")
  Call<ResponseBody> update_info(@Field("id_informasi") int id_informasi,
          @Field("sembuh") int sembuh,
          @Field("meninggal") int meninggal,
          @Field("positif") int positif,
          @Field("odp") int odp);
  
  @GET("account/index")
  Call<ResponseAccount> getUser();
  
  @FormUrlEncoded
  @POST("account/register")
  Call<ResponseBody> regrister(@Field("nama") String nama,
                                 @Field("jk") String jk,
                                 @Field("tanggallahir") String tanggallahir,
                                 @Field("alamat") String alamat,
                                 @Field("nik") String nik,
                                 @Field("username") String username,
                                 @Field("password") String password,
                                 @Field("pekerjaan") String pekerjaan,
                                 @Field("foto") String foto,
                                 @Field("role") String role);
  
  @DELETE("account/delete/{username}")
  Call<ResponseBody> deleteUser(@Path("username")String username);
  
  @FormUrlEncoded
    @POST("account/update")
    Call<ResponseBody> update(@Field("nama") String nama,
                              @Field("jk") String jk,
                              @Field("tanggallahir") String tanggallahir,
                              @Field("alamat") String alamat,
                              @Field("nik") String nik,
                              @Field("username") String username,
                              @Field("password") String password,
                              @Field("pekerjaan") String pekerjaan,
                              @Field("foto") String foto);
    
    @FormUrlEncoded
    @POST("cekKesehatan/create")
    Call<ResponseBody> cekKesehata(@Field("gejala") String gejala,
                                   @Field("aktivitasi")String aktivitas,
                                   @Field("username") String username,
                                   @Field("hasil") int hasil);
    
    
    @GET("keluhan/index")
    Call<ResponseKeluhan> getKeluhan();

    @FormUrlEncoded
    @POST("keluhan/store")
    Call<ResponseBody> addKeluhan(@Field("pengirim") String pengirim,
                                  @Field("kategori") String kategori,
                                  @Field("tanggal") String tanggal,
                                  @Field("isi") String isi,
                                  @Field("status") String status);
}
