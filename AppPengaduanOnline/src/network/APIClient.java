/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author Kevin
 */
public class APIClient {
    public static final String BASE_URL = "http://192.168.43.117:/WebServices/Web_Services_Lapcovid19/api/";

    private static Retrofit mRetrofit;

    public static Retrofit getRetrofit(){
        if (mRetrofit==null){
            mRetrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return mRetrofit;
    }
}
