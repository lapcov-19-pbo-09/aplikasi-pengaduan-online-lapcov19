/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package administrator.beranda;

import entity.CekKesehatan;
import entity.InformasiCovid;
import entity.Keluhan;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import model.ResponseCekKesehatan;
import model.ResponseInformasiCovid;
import model.ResponseKeluhan;
import network.APIClient;
import network.APIServices;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class BerandaadminController implements Initializable {

    @FXML
    private TableView<InformasiCovid> table;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  sembuh;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  meninggal;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  pos;
    
    @FXML
    private TableColumn<InformasiCovid, Integer>  odp;
    
    @FXML
    private TableView<CekKesehatan> tHasilCek;
    
    @FXML
    private TableColumn<CekKesehatan, String>  cUser;
    
    @FXML
    private TableColumn<CekKesehatan, String>  cJK;
    
    @FXML
    private TableColumn<CekKesehatan, String>  cHa;
    
    @FXML
    private TableView<Keluhan> tKeluhan;
    
    @FXML
    private TableColumn<Keluhan, String>  cUsername;
    
    @FXML
    private TableColumn<Keluhan, String>  cKeluhan;
    
    @FXML
    private TextField rSem, rMen, rPos, rOdp;
    
    @FXML
    Button bAdd;
    
    @FXML 
    private LineChart<String,Number> lineChart;
     
    @FXML
    private CategoryAxis cAxis;
     
    @FXML
    private NumberAxis nAxis;
    
    ObservableList<InformasiCovid> oblist = FXCollections.observableArrayList();
    
    ObservableList<CekKesehatan> oblist2 = FXCollections.observableArrayList();
    
     ObservableList<Keluhan> oblistn = FXCollections.observableArrayList();
    
     int id = 0;
     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getData();
        dataBarchart();
        tableHasilCek();
        tableKeluhan();
    }    
    
    public void tableKeluhan(){
        APIServices services = APIClient.getRetrofit().create(APIServices.class);
         Call<ResponseKeluhan> call = services.getKeluhan();
         
         call.enqueue(new Callback<ResponseKeluhan>(){
            @Override
            public void onResponse(Call<ResponseKeluhan> call, Response<ResponseKeluhan> rspns) {
                 if(rspns.isSuccessful()){
                        int size = rspns.body().getKeluhan().size();
                        List<Keluhan> s = rspns.body().getKeluhan();
                        
                        for(int i=0;i<size;i++){
                            oblistn.add(new Keluhan(s.get(i).getId_keluhan(), s.get(i).getPengirim(), s.get(i).getKategori(),
                            s.get(i).getTanggal(), s.get(i).getIsi(), s.get(i).getStatus()));
                        }
                        
                        cUsername.setCellValueFactory(new PropertyValueFactory<>("pengirim"));
                        cKeluhan.setCellValueFactory(new PropertyValueFactory<>("isi"));
                        
                        tKeluhan.setItems(oblistn);
                        
                 }
            }

            @Override
            public void onFailure(Call<ResponseKeluhan> call, Throwable thrwbl) {
                System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
            }
             
         });
    }
    
     public void dataBarchart(){
          APIServices services = APIClient.getRetrofit().create(APIServices.class);
          Call<ResponseInformasiCovid> call = services.getInfoCovid();
          
          call.enqueue(new Callback<ResponseInformasiCovid>(){
              @Override
              public void onResponse(Call<ResponseInformasiCovid> call, Response<ResponseInformasiCovid> rspns) {
                   if(rspns.isSuccessful()){
                        int size = rspns.body().getInformasi_covid().size();
                        List<InformasiCovid> s = rspns.body().getInformasi_covid();
                       
//                        cAxis = new CategoryAxis();
                        Platform.runLater(() -> {
                            cAxis.setLabel("Id");
                            
    //                        nAxis = new NumberAxis();
                            nAxis.setLabel("Jumlah");
                          });
//                        lineChart = new LineChart<String,Number>(cAxis,nAxis);
                        
                        XYChart.Series series1 = new XYChart.Series();
                        series1.setName("Sembuh");
                     
                        XYChart.Series series2 = new XYChart.Series();
                        series2.setName("Meninggal");
                        
                        XYChart.Series series3 = new XYChart.Series();
                        series3.setName("Positif");
                        
                        XYChart.Series series4 = new XYChart.Series();
                        series4.setName("ODP");
                        
                        for(int i=0;i<size;i++){
                          series1.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getSembuh()));
                          series2.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getMeninggal()));
                          series3.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getPositif()));
                          series4.getData().add(new XYChart.Data(String.valueOf(i+1), s.get(i).getOdp()));
                        }
                        
                         Platform.runLater(() -> {
                            lineChart.setTitle("Informasi Covid");
                            lineChart.getData().addAll(series1, series2, series3, series4);
                       });
                   }else{
                        System.out.println(rspns.message());
                    }
              }

              @Override
              public void onFailure(Call<ResponseInformasiCovid> call, Throwable thrwbl) {
                  System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
              }
              
          });
     }
    
    public void getData(){
        APIServices services = APIClient.getRetrofit().create(APIServices.class);
        Call<ResponseInformasiCovid> call = services.getInfoCovid();
      
        call.enqueue(new Callback<ResponseInformasiCovid>(){
            @Override
            public void onResponse(Call<ResponseInformasiCovid> call, Response<ResponseInformasiCovid> rspns) {
                 if(rspns.isSuccessful()){
                      int size = rspns.body().getInformasi_covid().size();
                      List<InformasiCovid> s = rspns.body().getInformasi_covid();
                      
                        for(int i=0;i<size;i++){
                            oblist.add(new InformasiCovid(s.get(i).getId(), s.get(i).getSembuh(), s.get(i).getMeninggal(),
                                    s.get(i).getPositif(), s.get(i).getOdp()));
                        }
                        
                        sembuh.setCellValueFactory(new PropertyValueFactory<>("sembuh"));
                        meninggal.setCellValueFactory(new PropertyValueFactory<>("meninggal"));
                        pos.setCellValueFactory(new PropertyValueFactory<>("positif"));
                        odp.setCellValueFactory(new PropertyValueFactory<>("odp"));
                        
                        
                        table.setItems(oblist);
                      
                 }else{
                    System.out.println(rspns.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseInformasiCovid> call, Throwable thrwbl) {
              System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
            }
            
        });
    }
    
    public void logout(ActionEvent event) throws Exception{
        Preferences data = Preferences.userRoot();

            data.remove("username");
            data.remove("role");
         try{
         FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/FXMLDocument.fxml"));
         Parent parent1 = pinda.load();

         Scene x = new Scene(parent1);
         Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
         myStage.setScene(x);
         myStage.centerOnScreen();
         myStage.show();

  
         }catch(Exception e){
           e.printStackTrace();
         }
    }
    
     public void edit(ActionEvent event) throws Exception{
         if(table.getSelectionModel().getSelectedItem()!=null){
             InformasiCovid select = table.getSelectionModel().getSelectedItem();
             
             rSem.setText(String.valueOf(select.getSembuh()));
             rMen.setText(String.valueOf(select.getMeninggal()));
             rPos.setText(String.valueOf(select.getPositif()));
             rOdp.setText(String.valueOf(select.getOdp()));
             
             id = select.getId();
             
             bAdd.setText("Simpan");
             
            
         }else{
                JOptionPane.showMessageDialog(null, "Pilih salah satu table", "WARNING", JOptionPane.WARNING_MESSAGE);
           }
     }
    
    
    
    public void tambah(ActionEvent event) throws Exception{
        if(table.getSelectionModel().getSelectedItem()!=null){
              APIServices services = APIClient.getRetrofit().create(APIServices.class);
              services.update_info(id, Integer.parseInt(rSem.getText().toString()), Integer.parseInt(rMen.getText().toString()),
                Integer.parseInt(rPos.getText().toString()), Integer.parseInt(rOdp.getText().toString()))
                      .enqueue(new Callback<ResponseBody>(){
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                                 if(rspns.isSuccessful()){
                                   Platform.runLater(() -> {   
                                   try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
                              });
                        }else{
                           System.out.println(rspns.message());
                        }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                             System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                            }
                          
                      });
        }else{
        APIServices services = APIClient.getRetrofit().create(APIServices.class);
        services.tambah_info(Integer.parseInt(rSem.getText().toString()), Integer.parseInt(rMen.getText().toString()),
                Integer.parseInt(rPos.getText().toString()), Integer.parseInt(rOdp.getText().toString()))
                .enqueue(new Callback<ResponseBody>(){
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                     if(rspns.isSuccessful()){
                          Platform.runLater(() -> {   
                            try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
                              });
                        }else{
                           System.out.println(rspns.message());
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                            System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                    }
                    
                });
        }
                
    }
    
     public void hapus(ActionEvent event) throws Exception{
         if(table.getSelectionModel().getSelectedItem()!=null){
              InformasiCovid select = table.getSelectionModel().getSelectedItem();
              APIServices services = APIClient.getRetrofit().create(APIServices.class);
              
              services.deleteInfo(select.getId())
                      .enqueue(new Callback<ResponseBody>(){
                  @Override
                  public void onResponse(Call<ResponseBody> call, Response<ResponseBody> rspns) {
                         if(rspns.isSuccessful()){
                            Platform.runLater(() -> {   
                            try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
                              });
                        }else{
                           System.out.println(rspns.message());
                        }
                  }

                  @Override
                  public void onFailure(Call<ResponseBody> call, Throwable thrwbl) {
                      System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
                  }
                          
                      });
              
         }else{
                JOptionPane.showMessageDialog(null, "Pilih salah satu table", "WARNING", JOptionPane.WARNING_MESSAGE);
           }
     }
     
     public void tableHasilCek(){
         APIServices services = APIClient.getRetrofit().create(APIServices.class);
         Call<ResponseCekKesehatan> call = services.getHasilCek();
         
         call.enqueue(new Callback<ResponseCekKesehatan>(){
             @Override
             public void onResponse(Call<ResponseCekKesehatan> call, Response<ResponseCekKesehatan> rspns) {
                 if(rspns.isSuccessful()){
                   Platform.runLater(() -> { 
                      int size = rspns.body().getCekKesehatan().size();
                      List<CekKesehatan> s = rspns.body().getCekKesehatan();
                      
                      for(int i=0;i<size;i++){
                            oblist2.add(new CekKesehatan(s.get(i).getId_cek(), s.get(i).getDaftarpertanyaan_gejala(),
                                    s.get(i).getDaftarpertanyaan_aktivitas(), s.get(i).getUsername(), s.get(i).getHasil(),
                                    s.get(i).getJns(), s.get(i).getHas()));
                    
                                 
                      }
                      
                      cUser.setCellValueFactory(new PropertyValueFactory<>("username"));
                      cJK.setCellValueFactory(new PropertyValueFactory<>("jns"));
                      cHa.setCellValueFactory(new PropertyValueFactory<>("has"));
                      
                      tHasilCek.setItems(oblist2);
                          });
                 }else{
                    System.out.println(rspns.message());
                }
             }

             @Override
             public void onFailure(Call<ResponseCekKesehatan> call, Throwable thrwbl) {
                  System.out.println("Something went wrong...Error message: "+ thrwbl.getMessage());
             }
             
         });
     }
    
         
     public void exit(ActionEvent event) throws Exception{
            Preferences data = Preferences.userRoot();

              Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
              myStage.centerOnScreen();
              myStage.close();
      }
     
      public void about(ActionEvent event) throws Exception{
                                    try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/about/about.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
      }
      
       public void tambahUser(ActionEvent event) throws Exception{
                                    try{
                                    FXMLLoader pinda = new FXMLLoader(getClass().getResource("/apppengaduanonline/TambahUser.fxml"));
                                      Parent parent1 = pinda.load(); 
                                      Scene x = new Scene(parent1);
                                      Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                      myStage.setScene(x);
                                      myStage.centerOnScreen();
                                      myStage.show();

                                          }
                                          catch(Exception e){
                                              e.printStackTrace();
                                          } 
      }
   
}
