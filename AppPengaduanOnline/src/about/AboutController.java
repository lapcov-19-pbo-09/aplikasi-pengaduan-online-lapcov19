/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package about;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Kevin
 */
public class AboutController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    public void kembali(ActionEvent event) throws Exception{
        Preferences userPreferences = Preferences.userRoot();
       String role = userPreferences.get("role", null);
       
       if(role.equals("pengurus")){
         try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/administrator/beranda/berandaadmin.fxml"));
                                        Parent parent1 = pinda.load();

                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
       }else if(role.equals("penduduk")){
            try{
                                        FXMLLoader pinda = new FXMLLoader(getClass().getResource("/user/beranda/berandauser.fxml"));
                                        Parent parent1 = pinda.load();

                                        Scene x = new Scene(parent1);
                                        Stage myStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                                        myStage.setScene(x);
                                        myStage.centerOnScreen();
                                        myStage.show();
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
       }
     }
}
