/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Kevin
 */
public class InformasiCovid {
    @SerializedName("id")
    @Expose
    private int id;
    
    @SerializedName("sembuh")
    @Expose
    private int sembuh;
    
    @SerializedName("meninggal")
    @Expose
    private int meninggal;
     
    
    @SerializedName("positif")
    @Expose
    private int positif;
      
    @SerializedName("odp")
    @Expose
    private int odp;

    public InformasiCovid(int id, int sembuh, int meninggal, int positif, int odp) {
        this.id = id;
        this.sembuh = sembuh;
        this.meninggal = meninggal;
        this.positif = positif;
        this.odp = odp;
    }
    
    public int getMeninggal() {
        return meninggal;
    }

    public void setMeninggal(int meninggal) {
        this.meninggal = meninggal;
    }    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSembuh() {
        return sembuh;
    }

    public void setSembuh(int sembuh) {
        this.sembuh = sembuh;
    }

    public int getPositif() {
        return positif;
    }

    public void setPositif(int positif) {
        this.positif = positif;
    }

    public int getOdp() {
        return odp;
    }

    public void setOdp(int odp) {
        this.odp = odp;
    }
    
    
}
