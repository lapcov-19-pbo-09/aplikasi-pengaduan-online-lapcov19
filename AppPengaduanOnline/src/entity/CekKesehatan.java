/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *
 * @author Kevin
 */
public class CekKesehatan {
    @SerializedName("id_cek")
    private int id_cek;

    @SerializedName("daftarpertanyaan_gejala")
    private String daftarpertanyaan_gejala;

    @SerializedName("daftarpertanyaan_aktivitas")
    private String daftarpertanyaan_aktivitas;

    @SerializedName("username")
    private String username;

    @SerializedName("hasil")
    private int hasil;
    
    String jns;
    
    String has;

    public CekKesehatan(int id_cek, String daftarpertanyaan_gejala, String daftarpertanyaan_aktivitas, String username, int hasil, String jns, String has) {
        this.id_cek = id_cek;
        this.daftarpertanyaan_gejala = daftarpertanyaan_gejala;
        this.daftarpertanyaan_aktivitas = daftarpertanyaan_aktivitas;
        this.username = username;
        this.hasil = hasil;
        this.jns = jns;
        this.has = has;
        
    }

    public String getHas() {
        if(getHasil()==0){
            return "Rendah";
        }else if(getHasil()>0&&getHasil()<=2){
            return "Sedang";
        }else if(getHasil()>=2){
            return "Tinggi";
        }else
           return "-";
    }

    public void setHas(String has) {
        this.has = has;
    }
    
    
     public String jns() {
        return jns;
    }

    public String getJns() {
        if(getDaftarpertanyaan_gejala().equals("1")){
            return "Gejala";
        }else if(getDaftarpertanyaan_aktivitas().equals("1")){
            return "Aktivitas";
        }else  return "Aktivitas";
    }
    
    public int getId_cek() {
        return id_cek;
    }

    public void setId_cek(int id_cek) {
        this.id_cek = id_cek;
    }

    public String getDaftarpertanyaan_gejala() {
        return daftarpertanyaan_gejala;
    }

    public void setDaftarpertanyaan_gejala(String daftarpertanyaan_gejala) {
        this.daftarpertanyaan_gejala = daftarpertanyaan_gejala;
    }

    public String getDaftarpertanyaan_aktivitas() {
        return daftarpertanyaan_aktivitas;
    }

    public void setDaftarpertanyaan_aktivitas(String daftarpertanyaan_aktivitas) {
        this.daftarpertanyaan_aktivitas = daftarpertanyaan_aktivitas;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getHasil() {
        return hasil;
    }

    public void setHasil(int hasil) {
        this.hasil = hasil;
    }
    
    
    
}
