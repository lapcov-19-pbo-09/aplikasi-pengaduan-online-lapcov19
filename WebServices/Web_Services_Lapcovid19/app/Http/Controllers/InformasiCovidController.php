<?php

namespace App\Http\Controllers;

use App\informasi_covid;
use Illuminate\Http\Request;

class InformasiCovidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $informasi = informasi_covid::all();
        return response()->json([
            'informasi_covid'=>$informasi
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $informasi_covid = new informasi_covid;

        $informasi_covid->sembuh = $request->sembuh;
        $informasi_covid->meninggal = $request->meninggal;
        $informasi_covid->positif = $request->positif;
        $informasi_covid->odp = $request->odp;

        $save = $informasi_covid->save();

        if(!$save){
            App::abort(500, 'Error');
        }else{
            $result["success"] = "1";
            $result["message"] = "success";
            echo json_encode($result);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\informasi_covid  $informasi_covid
     * @return \Illuminate\Http\Response
     */
    public function show(informasi_covid $informasi_covid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\informasi_covid  $informasi_covid
     * @return \Illuminate\Http\Response
     */
    public function edit(informasi_covid $informasi_covid)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\informasi_covid  $informasi_covid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id_informasi =  $request->id_informasi;
        $informasi_covid = informasi_covid::find($id_informasi);

        $informasi_covid->sembuh = $request->sembuh;
        $informasi_covid->meninggal = $request->meninggal;
        $informasi_covid->positif = $request->positif;
        $informasi_covid->odp = $request->odp;

        $update = $informasi_covid->update();

        if(!$update){
            App::abort(500, 'Error');
        }else{
            $result["success"] = "1";
            $result["message"] = "success";
            echo json_encode($result);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\informasi_covid  $informasi_covid
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_informasi)
    {
        $informasi_covid = informasi_covid::find($id_informasi);
        $remove = $informasi_covid->delete();

        if(!$remove){
            App::abort(500, 'Error');
        }else{
            $result["success"] = "1";
            $result["message"] = "success";
            echo json_encode($result);
        }
        
    }
}
